#ifndef SAMP_PIMPL_H
#define SAMP_PIMPL_H

#include "SAMP/Library.h"

namespace SAMP {
	class Base;
	class Chat;
	class Input;
	class Dialog;
	class DXUT;
	class Fonts;
	class ObjectPool;
	class PlayerPool;
	class Pools;
	class RemotePlayer;
	class VehiclePool;
	class Memory;
	class Misc;
	class ScoreBoard;
} // namespace SAMP

#endif // SAMP_PIMPL_H
