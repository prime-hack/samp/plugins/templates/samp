#include "Memory.h"
#include "Library.h"
#include <stdexcept>

SAMP::Memory *SAMP::Memory::self = nullptr;

SAMP::Memory::Memory() {
	switch ( Version() ) {
		case eVerCode::notLoaded:
			throw std::runtime_error( "SAMP not loaded" );
			break;
		case eVerCode::R1:
			pFree = reinterpret_cast<decltype( pFree )>( Library() + 0xB52D5 );
			pCalloc = reinterpret_cast<decltype( pCalloc )>( Library() + 0xB521A );
			pMalloc = reinterpret_cast<decltype( pMalloc )>( Library() + 0xB5454 );
			pRealloc = reinterpret_cast<decltype( pRealloc )>( Library() + 0xB5A46 );
			pDeallocPacket = reinterpret_cast<decltype( pDeallocPacket )>( Library() + 0x34AC0 );
			pAllocPacket = reinterpret_cast<decltype( pAllocPacket )>( Library() + 0x347E0 );
			pNew = reinterpret_cast<decltype( pNew )>( Library() + 0xB4B12 );
			pDelete = reinterpret_cast<decltype( pDelete )>( Library() + 0xB52D5 );
			break;
		case eVerCode::R3:
			pFree = reinterpret_cast<decltype( pFree )>( Library() + 0xC7275 );
			pCalloc = reinterpret_cast<decltype( pCalloc )>( Library() + 0xC71BA );
			pMalloc = reinterpret_cast<decltype( pMalloc )>( Library() + 0xC73F4 );
			pRealloc = reinterpret_cast<decltype( pRealloc )>( Library() + 0xC79E6 );
			pDeallocPacket = reinterpret_cast<decltype( pDeallocPacket )>( Library() + 0x37E70 );
			pAllocPacket = reinterpret_cast<decltype( pAllocPacket )>( Library() + 0x37B90 );
			pNew = reinterpret_cast<decltype( pNew )>( Library() + 0xC6B0A );
			pDelete = reinterpret_cast<decltype( pDelete )>( Library() + 0xC7275 );
			break;
		case eVerCode::R4:
			pFree = reinterpret_cast<decltype( pFree )>( Library() + 0xC6A5D );
			pCalloc = reinterpret_cast<decltype( pCalloc )>( Library() + 0xC69A2 );
			pMalloc = reinterpret_cast<decltype( pMalloc )>( Library() + 0xC6BDC );
			pRealloc = reinterpret_cast<decltype( pRealloc )>( Library() + 0xC71C6 );
			pDeallocPacket = reinterpret_cast<decltype( pDeallocPacket )>( Library() + 0x38560 );
			pAllocPacket = reinterpret_cast<decltype( pAllocPacket )>( Library() + 0x38280 );
			break;
		default:
			throw std::runtime_error( "Unsupported SAMP version" );
			break;
	}

	++refCounter;
}

SAMP::Memory *SAMP::Memory::Instance( bool no_create ) {
	if ( !self && !no_create ) self = new Memory();
	return self;
}

SAMP::Memory *SAMP::Memory::InstanceRef() {
	auto instance = Instance();
	if ( instance != nullptr ) ++instance->refCounter;
	return instance;
}

void SAMP::Memory::DeleteInstance( bool force ) {
	if ( !self ) return;
	--self->refCounter;
	if ( !self->refCounter || force ) {
		delete self;
		self = nullptr;
	}
}

void SAMP::Memory::free( void *lpMem ) {
	if ( !pFree ) return;
	pFree( lpMem );
}

void *SAMP::Memory::calloc( int count, int size ) {
	if ( !pCalloc ) return nullptr;
	return pCalloc( count, size );
}

void *SAMP::Memory::malloc( int size ) {
	if ( !pMalloc ) return nullptr;
	return pMalloc( size );
}

void *SAMP::Memory::realloc( void *lpMem, int size ) {
	if ( !pRealloc ) return nullptr;
	return pRealloc( lpMem, size );
}

void SAMP::Memory::DeallocPacket( Packet *packet ) {
	if ( !pDeallocPacket ) return;
	return pDeallocPacket( packet );
}

Packet *SAMP::Memory::AllocPacket( int count ) {
	if ( !pAllocPacket ) return nullptr;
	return pAllocPacket( count );
}

void *SAMP::Memory::NewChars( int size ) {
	if ( !pNew ) return nullptr;
	return pNew( size );
}

void SAMP::Memory::DeleteChars( void *lpMem ) {
	if ( !pDelete ) return;
	return pDelete( lpMem );
}
