#include "VehiclePool.h"
#include "Library.h"
#include <stdexcept>
#include <windows.h>

SAMP::VehiclePool::VehiclePool( void *pool ) : pVehiclePool( pool ) {
	switch ( Version() ) {
		case eVerCode::R1:
			[[fallthrough]];
		case eVerCode::R4:
			[[fallthrough]];
		case eVerCode::R3:
			pListed = reinterpret_cast<int *>( (size_t)pVehiclePool + 12404 );
			pVehs	= reinterpret_cast<CVehicle **>( (size_t)pVehiclePool + 20404 );
			break;
		default:
			throw std::runtime_error( "Unsupported SAMP version" );
			break;
	}
}
