#include "DXUT.h"
#include "Library.h"
#include <stdexcept>
#ifdef __clang__
#	include "cp1251.hpp"
#endif

SAMP::DXUT *SAMP::DXUT::self = nullptr;

SAMP::DXUT::DXUT() {
	switch ( Version() ) {
		case eVerCode::notLoaded:
			throw std::runtime_error( "SAMP not loaded" );
			break;
		case eVerCode::R1:
			pAddStatic		= reinterpret_cast<decltype( pAddStatic )>( Library() + 0x8C610 );
			pAddButton		= reinterpret_cast<decltype( pAddButton )>( Library() + 0x8C6B0 );
			pAddCheckBox	= reinterpret_cast<decltype( pAddCheckBox )>( Library() + 0x8C750 );
			pAddRadioButton = reinterpret_cast<decltype( pAddRadioButton )>( Library() + 0x8C800 );
			pAddComboBox	= reinterpret_cast<decltype( pAddComboBox )>( Library() + 0x8CDF0 );
			pAddSlider		= reinterpret_cast<decltype( pAddSlider )>( Library() + 0x8C8C0 );
			pAddEditBox		= reinterpret_cast<decltype( pAddEditBox )>( Library() + 0x8CA50 );
			pRequestFocus	= reinterpret_cast<decltype( pRequestFocus )>( Library() + 0x80410 );
			pClearFocus		= reinterpret_cast<decltype( pClearFocus )>( Library() + 0x807E0 );
			pRemoveControl	= reinterpret_cast<decltype( pRemoveControl )>( Library() + 0x828D0 );
			pGetControl		= reinterpret_cast<decltype( pGetControl )>( Library() + 0x82C50 );
			s_pControlFocus = reinterpret_cast<decltype( s_pControlFocus )>( SAMP::Library() + 0x12E350 );
			break;
		case eVerCode::R3:
			pAddStatic		= reinterpret_cast<decltype( pAddStatic )>( Library() + 0x90560 );
			pAddButton		= reinterpret_cast<decltype( pAddButton )>( Library() + 0x90600 );
			pAddCheckBox	= reinterpret_cast<decltype( pAddCheckBox )>( Library() + 0x906A0 );
			pAddRadioButton = reinterpret_cast<decltype( pAddRadioButton )>( Library() + 0x90750 );
			pAddComboBox	= reinterpret_cast<decltype( pAddComboBox )>( Library() + 0x90D40 );
			pAddSlider		= reinterpret_cast<decltype( pAddSlider )>( Library() + 0x90810 );
			pAddEditBox		= reinterpret_cast<decltype( pAddEditBox )>( Library() + 0x909A0 );
			pRequestFocus	= reinterpret_cast<decltype( pRequestFocus )>( Library() + 0x84320 );
			pClearFocus		= reinterpret_cast<decltype( pClearFocus )>( Library() + 0x846F0 );
			pRemoveControl	= reinterpret_cast<decltype( pRemoveControl )>( Library() + 0x867E0 );
			pGetControl		= reinterpret_cast<decltype( pGetControl )>( Library() + 0x86B60 );
			s_pControlFocus = reinterpret_cast<decltype( s_pControlFocus )>( SAMP::Library() + 0x1424D0 );
			break;
		case eVerCode::R4:
			pAddStatic		= reinterpret_cast<decltype( pAddStatic )>( Library() + 0x90CA0 );
			pAddButton		= reinterpret_cast<decltype( pAddButton )>( Library() + 0x90600 );
			pAddCheckBox	= reinterpret_cast<decltype( pAddCheckBox )>( Library() + 0x90DE0 );
			pAddRadioButton = reinterpret_cast<decltype( pAddRadioButton )>( Library() + 0x90E90 );
			pAddComboBox	= reinterpret_cast<decltype( pAddComboBox )>( Library() + 0x91480 );
			pAddSlider		= reinterpret_cast<decltype( pAddSlider )>( Library() + 0x90F50 );
			pAddEditBox		= reinterpret_cast<decltype( pAddEditBox )>( Library() + 0x910E0 );
			pRequestFocus	= reinterpret_cast<decltype( pRequestFocus )>( Library() + 0x84A60 );
			pClearFocus		= reinterpret_cast<decltype( pClearFocus )>( Library() + 0x84E30 );
			pRemoveControl	= reinterpret_cast<decltype( pRemoveControl )>( Library() + 0x86F20 );
			pGetControl		= reinterpret_cast<decltype( pGetControl )>( Library() + 0x872A0 );
			s_pControlFocus = reinterpret_cast<decltype( s_pControlFocus )>( SAMP::Library() + 0x1425F8 );
			break;
		default:
			throw std::runtime_error( "Unsupported SAMP version" );
			break;
	}
	++refCounter;
}

SAMP::DXUT *SAMP::DXUT::Instance( bool no_create ) {
	if ( !self && !no_create ) self = new DXUT();
	return self;
}

SAMP::DXUT *SAMP::DXUT::InstanceRef() {
	auto instance = Instance();
	if ( instance != nullptr ) ++instance->refCounter;
	return instance;
}

void SAMP::DXUT::DeleteInstance( bool force ) {
	if ( !self ) return;
	--self->refCounter;
	if ( !self->refCounter || force ) {
		delete self;
		self = nullptr;
	}
}

int SAMP::DXUT::AddStatic( CDXUTDialog *dialog, int ID, const char *strText, int x, int y, int w, int h,
						   bool bIsDefault, CDXUTStatic **ppCreated ) {
	if ( !pAddStatic ) return 0x8007000E;
#ifdef __clang__
	return pAddStatic( dialog, ID, cp1251str( strText ).data(), x, y, w, h, bIsDefault, ppCreated );
#else
	return pAddStatic( dialog, ID, strText, x, y, w, h, bIsDefault, ppCreated );
#endif
}

int SAMP::DXUT::AddButton( CDXUTDialog *dialog, int ID, const char *strText, int x, int y, int w, int h, int nHotKey,
						   bool bIsDefault, CDXUTButton **ppCreated ) {
	if ( !pAddButton ) return 0x8007000E;
#ifdef __clang__
	return pAddButton( dialog, ID, cp1251str( strText ).data(), x, y, w, h, nHotKey, bIsDefault, ppCreated );
#else
	return pAddButton( dialog, ID, strText, x, y, w, h, nHotKey, bIsDefault, ppCreated );
#endif
}

int SAMP::DXUT::AddCheckBox( CDXUTDialog *dialog, int ID, const char *strText, int x, int y, int w, int h,
							 bool bChecked, int nHotKey, bool bIsDefault, CDXUTCheckBox **ppCreated ) {
	if ( !pAddCheckBox ) return 0x8007000E;
#ifdef __clang__
	return pAddCheckBox( dialog, ID, cp1251str( strText ).data(), x, y, w, h, bChecked, nHotKey, bIsDefault,
						 ppCreated );
#else
	return pAddCheckBox( dialog, ID, strText, x, y, w, h, bChecked, nHotKey, bIsDefault, ppCreated );
#endif
}

int SAMP::DXUT::AddRadioButton( CDXUTDialog *dialog, int ID, int nButtonGroup, const char *strText, int x, int y, int w,
								int h, bool bChecked, int nHotKey, bool bIsDefault, CDXUTRadioButton **ppCreated ) {
	if ( !pAddRadioButton ) return 0x8007000E;
#ifdef __clang__
	return pAddRadioButton( dialog, ID, nButtonGroup, cp1251str( strText ).data(), x, y, w, h, bChecked, nHotKey,
							bIsDefault, ppCreated );
#else
	return pAddRadioButton( dialog, ID, nButtonGroup, strText, x, y, w, h, bChecked, nHotKey, bIsDefault, ppCreated );
#endif
}

int SAMP::DXUT::AddComboBox( CDXUTDialog *dialog, int ID, int x, int y, int w, int h, int nHotKey, bool bIsDefault,
							 CDXUTComboBox **ppCreated ) {
	if ( !pAddComboBox ) return 0x8007000E;
	return pAddComboBox( dialog, ID, x, y, w, h, nHotKey, bIsDefault, ppCreated );
}

int SAMP::DXUT::AddSlider( CDXUTDialog *dialog, int ID, int x, int y, int w, int h, int min, int max, int value,
						   bool bIsDefault, CDXUTSlider **ppCreated ) {
	if ( !pAddSlider ) return 0x8007000E;
	return pAddSlider( dialog, ID, x, y, w, h, min, max, value, bIsDefault, ppCreated );
}

int SAMP::DXUT::AddEditBox( CDXUTDialog *dialog, int ID, const char *strText, int x, int y, int w, int h,
							bool bIsDefault, CDXUTEditBox **ppCreated ) {
	if ( !pAddEditBox ) return 0x8007000E;
#ifdef __clang__
	return pAddEditBox( dialog, ID, cp1251str( strText ).data(), x, y, w, h, bIsDefault, ppCreated );
#else
	return pAddEditBox( dialog, ID, strText, x, y, w, h, bIsDefault, ppCreated );
#endif
}

void SAMP::DXUT::RequestFocus( CDXUTControl *pControl ) {
	if ( !pRequestFocus ) return;
	pRequestFocus( nullptr, pControl ); // статичный метод
}

void SAMP::DXUT::ClearFocus() {
	if ( !pClearFocus ) return;
	pClearFocus();
}

void SAMP::DXUT::RemoveControl( CDXUTDialog *dialog, int ID ) {
	if ( !pRequestFocus ) return;
	pRemoveControl( dialog, ID );
}

CDXUTControl *SAMP::DXUT::GetControl( CDXUTDialog *dialog, int ID, int type ) {
	if ( !pGetControl ) return nullptr;
	return pGetControl( dialog, ID, type );
}

bool SAMP::DXUT::IsFocused( CDXUTControl *control ) {
	if ( !s_pControlFocus ) return false;
	return s_pControlFocus == control;
}
