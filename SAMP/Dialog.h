#ifndef DIALOG_CHAT_H
#define DIALOG_CHAT_H

class CDXUTDialog;
class CDXUTListBox;
class CDXUTEditBox;

namespace SAMP {
	class Dialog {
		static Dialog *self;
		int			   refCounter = 0;

		void *pDialog	   = nullptr;
		int	  ofIsActive   = 0;
		int	  ofType	   = 0;
		int	  ofId		   = 0;
		int	  ofText	   = 0;
		int	  ofTitle	   = 0;
		int	  ofDxut	   = 0;
		int	  ofServerside = 0;
		int	  ofList	   = 0;
		int	  ofEdit	   = 0;
		int	  ofTextX	   = 0;
		int	  ofTextY	   = 0;
		int	  ofTextW	   = 0;
		int	  ofTextH	   = 0;
		int	  ofSizeW	   = 0;
		int	  ofSizeH	   = 0;

		Dialog();
		~Dialog() {}

	public:
		/**
		 * @brief Возвращает инстанс диалога SA:MP
		 * @details Если инстанс не существует и параметр \b no_create равен false, то создает новый инстанс
		 * @param no_create Пропустить создание инстанса, если он не существует
		 * @return инстанс диалога SA:MP
		 */
		[[nodiscard]] static Dialog *Instance( bool no_create = false );
		/**
		 * @brief Возвращает ссылку на инстанс диалога SA:MP
		 * @details Поведение подобно вызову метода Instance с аргументами по умолчанию, но дополнительно инкрементируется счетчик ссылок
		 * @return инстанс диалога SA:MP
		 */
		[[nodiscard]] static Dialog *InstanceRef();
		/**
		 * @brief Удаляет инстанс диалога SA:MP
		 * @details Если счетчик ссылок не пустой, то вместо удаления будет декрементирован счетчик. Для принудительного удаления инстанса, используйте force = true
		 * @param force Принудительное удаление
		 */
		static void DeleteInstance( bool force = false );

		/// Проверяет, активен ли диалог
		[[nodiscard]] bool isActive() const;

		// Устанавливает активность диалога
		void setActive( bool state );

		/// Возвращает тип диалога
		[[nodiscard]] int type() const;

		/// Проверяет, серверный ли диалог
		[[nodiscard]] bool isRemote() const;

		/// Возвращает id диалога
		[[nodiscard]] int id() const;

		/// Возвращает текст диалога
		[[nodiscard]] char *text() const;

		/**
		 * @brief Устанавливает новый текст в диалоге
		 * @details старый текст перед этим должен быть освобожден (Memory::free)
		 * @param text указатель на текст
		 */
		void setText( char *text ) const;

		/// Возвращает позицию текст по X
		[[nodiscard]] int &textPosX();

		/// Возвращает позицию текст по Y
		[[nodiscard]] int &textPosY();

		/// Возвращает ширину текста
		[[nodiscard]] int &textW();

		/// Возвращает высоту текста
		[[nodiscard]] int &textH();

		/// Возвращает ширину диалога
		[[nodiscard]] int &sizeW();

		/// Возвращает высоту диалога
		[[nodiscard]] int &sizeH();

		/// Возвращает заголовок диалога
		[[nodiscard]] char *title() const;

		/// Возвращает диалог
		[[nodiscard]] CDXUTDialog *dialog() const;

		/// Возвращает список диалога
		[[nodiscard]] CDXUTListBox *list() const;

		/// Возвращает поле ввода диалога
		[[nodiscard]] CDXUTEditBox *edit() const;

		/// Возвращает Указатель на самповскую структуру
		[[nodiscard]] void *context() const;
	};

} // namespace SAMP

#endif // DIALOG_CHAT_H
