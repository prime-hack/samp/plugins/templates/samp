#include "ScoreBoard.h"
#include "Library.h"
#include <stdexcept>

SAMP::ScoreBoard *SAMP::ScoreBoard::self = nullptr;

SAMP::ScoreBoard::ScoreBoard() {
	switch ( Version() ) {
		case eVerCode::notLoaded:
			throw std::runtime_error( "SAMP not loaded" );
			break;
		case eVerCode::R1:
			pScoreBoard = reinterpret_cast<void *>( Library() + 0x21A0B4 );
			break;
		case eVerCode::R3:
			pScoreBoard = reinterpret_cast<void *>( Library() + 0x26E894 );
			break;
		case eVerCode::R4:
			pScoreBoard = reinterpret_cast<void *>( Library() + 0x26E894 );
			break;
		default:
			throw std::runtime_error( "Unsupported SAMP version" );
			break;
	}
	ofEnabled = 0x00;
	ofDialog  = 0x34;
	ofList	  = 0x38;

	++refCounter;
}

SAMP::ScoreBoard *SAMP::ScoreBoard::Instance( bool no_create ) {
	if ( !self && !no_create ) self = new ScoreBoard();
	return self;
}

SAMP::ScoreBoard *SAMP::ScoreBoard::InstanceRef() {
	auto instance = Instance();
	if ( instance != nullptr ) ++instance->refCounter;
	return instance;
}

void SAMP::ScoreBoard::DeleteInstance( bool force ) {
	if ( !self ) return;
	--self->refCounter;
	if ( !self->refCounter || force ) {
		delete self;
		self = nullptr;
	}
}

bool SAMP::ScoreBoard::isEnabled() const {
	if ( !pScoreBoard ) return false;
	return *(int *)( *(size_t *)pScoreBoard + ofEnabled );
}

CDXUTDialog *SAMP::ScoreBoard::dialog() const {
	if ( !pScoreBoard ) return nullptr;
	return *(CDXUTDialog **)( *(size_t *)pScoreBoard + ofDialog );
}

CDXUTListBox *SAMP::ScoreBoard::list() const {
	if ( !pScoreBoard ) return nullptr;
	return *(CDXUTListBox **)( *(size_t *)pScoreBoard + ofList );
}
