#ifndef INPUT_CHAT_H
#define INPUT_CHAT_H

#include <functional>
#include <string>
#include <string_view>
#include <stdexcept>

#include <llmo/SRHook.hpp>

class CDXUTDialog;
class CDXUTEditBox;

namespace SAMP {
	class Input {
		static Input *self;
		int			  refCounter = 0;

		void *		   pInput																		 = nullptr;
		int			   ofEnabled																	 = 0;
		int			   ofRecall																		 = 0;
		int			   ofRecallTotal																 = 0;
		int			   ofRecallCurrent																 = 0;
		int			   recallMaxLen																	 = 0;
		int			   ofDialog																		 = 0;
		int			   ofEdit																		 = 0;
		int			   ofCmdCount																	 = 0;
		int			   ofCmdNames																	 = 0;
		int			   ofCmdCbs																		 = 0;
		int			   cmdMaxLen																	 = 0;
		int			   cmdDefaultCount																 = 0;
		int			   cmdMaxCount																	 = 0;
		unsigned char *pJnzChatT																	 = nullptr;
		void( __stdcall *pSendCommand )( const char * )												 = nullptr;
		void( __thiscall *pCloseChat )( void *this_ )												 = nullptr;
		void( __thiscall *pOpenChat )( void *this_ )												 = nullptr;
		void( __thiscall *pAddCmd )( void *this_, const char *name_, void ( *cb_ )( const char * ) ) = nullptr;

		Input();
		~Input() {}

	public:
		/**
		 * @brief Возвращает инстанс ввода SA:MP
		 * @details Если инстанс не существует и параметр \b no_create равен false, то создает новый инстанс
		 * @param no_create Пропустить создание инстанса, если он не существует
		 * @return инстанс ввода SA:MP
		 */
		[[nodiscard]] static Input *Instance( bool no_create = false );
		/**
		 * @brief Возвращает ссылку на инстанс ввода SA:MP
		 * @details Поведение подобно вызову метода Instance с аргументами по умолчанию, но дополнительно инкрементируется счетчик ссылок
		 * @return инстанс ввода SA:MP
		 */
		[[nodiscard]] static Input *InstanceRef();
		/**
		 * @brief Удаляет инстанс ввода SA:MP
		 * @details Если счетчик ссылок не пустой, то вместо удаления будет декрементирован счетчик. Для принудительного удаления инстанса, используйте force = true
		 * @param force Принудительное удаление
		 */
		static void DeleteInstance( bool force = false );

		/// Проверяет, активно ли поле ввода чата
		[[nodiscard]] bool isInputEnabled() const;

		/**
		 * @brief Переключает активацию чата на T
		 * @param state Активировать ли чат на T
		 * @return Успешность переключения
		 */
		bool toggleChatT( bool state );

		/// Проверяет, активируется ли чат на T
		[[nodiscard]] bool isChatT() const;

		/// Возвращает количество сообщений в буффере ввода
		[[nodiscard]] int &recalls() const;

		/// Возвращает номер текущего выбранного сообщения в буффере историй ввода
		[[nodiscard]] int &currentRecall() const;

		/**
		 * @brief Возвращает сообщение из буффера ввода
		 * @param id номер сообщения
		 * @return сообщение
		 */
		[[nodiscard]] char *recall( int id ) const;

		/**
		 * @brief Отправляет команду на сервер
		 * @param cmd команда вместе с аргументами
		 */
		void sendCommand( const char *cmd );
		void sendCommand( std::string_view cmd );

		/// Возвращает диалог
		[[nodiscard]] CDXUTDialog *dialog() const;

		/// Возвращает поле ввода чата
		[[nodiscard]] CDXUTEditBox *edit() const;

		/// Возвращает количество зарегистрированных команд
		[[nodiscard]] int cmdCount() const;

		/**
		 * @brief Возвращает команду по ее id
		 * @param id команды
		 * @return команда
		 */
		[[nodiscard]] char *cmd( int id ) const;

		/**
		 * @brief Возвращает id команды
		 * @param cmd команда
		 * @return id
		 */
		[[nodiscard]] int cmdId( std::string_view cmd ) const;

		/**
		 * @brief Проверяет зарегистрирована ли команда
		 * @param cmd команда
		 */
		[[nodiscard]] bool isCmdRegistered( std::string_view cmd ) const;

		/// Тип тела команды
		typedef void ( *command_func_t )( const char * );

		/**
		 * @brief Регистрирует новую команду
		 * @param cmd команда
		 * @param cb тело команды
		 * @return \bfalse если не удалось зарегистрировать
		 */
		bool registerCommand( std::string_view cmd, const command_func_t &cb );

		/**
		 * @brief Удаляет команду
		 * @param cmd команда
		 * @return \false если не удалось удалить команду, или команда уже удалена
		 */
		bool unregisterCommand( std::string_view cmd );

		/**
		 * @brief Изменяет тело команды
		 * @param cmd команда
		 * @param cb новое тело
		 * @return указатель на старое тело или nullptr в случае ошибки
		 */
		command_func_t redirectCommand( std::string_view cmd, const command_func_t &cb );

		/**
		 * @brief Изменяет имя команды
		 * @param cmd команда
		 * @param new_cmd новое имя команды
		 * @return \bfalse если не удалось переименовать команду
		 */
		bool renameCommand( std::string_view cmd, std::string_view new_cmd );

		/// Закрывает чат
		void closeChat();
		/// Открывает чат
		void openChat();


		/// Класс для упрощенной регистрации команд
		class Command {
		public:
			/// Класс для вызова методов класса из C-функций
			template<class C> class Helper {
			public:
				Helper( C *obj, void ( C::*func )( const char * ) ) {
					try {
						cmd_caller = SRHook::Allocator().allocate( 19 );
						push( '\x51' );							 // push ecx
						pusha( '\x8b', '\x4c', '\x24', '\x08' ); // mov ecx, [esp+8]
						push( '\x51' );							 // push ecx
						pusha( '\xb9', (uint32_t)obj );			 // mov ecx, this
						auto relAddr =
							SRHook::Hook<>::getRelAddr( (size_t)cmd_caller.ptr + cmd_length, (size_t)fn2void( func ) );
						pusha( '\xe8', relAddr ); // call FogDist::command
						push( '\x59' );			  // pop ecx
						push( '\xc3' );			  // ret
					} catch ( std::exception &e ) {
						MessageBoxA( 0, e.what(), __PRETTY_FUNCTION__, 0 );
					}
				}
				~Helper() {
					try {
						SRHook::Allocator().deallocate( cmd_caller );
					} catch ( std::exception &e ) {
						MessageBoxA( 0, e.what(), __PRETTY_FUNCTION__, 0 );
					}
				}

				command_func_t c_func() const { return (command_func_t)cmd_caller.ptr; }

			private:
				SRHook::ptr_t cmd_caller{ nullptr, 0 };
				uint32_t	  cmd_length = 0;

				void push( uint8_t *data, size_t length ) {
					for ( int i = 0; i < length; ++i ) cmd_caller.ptr[cmd_length++] = data[i];
				}

				template<typename T> void push( const T &value ) {
					if constexpr ( sizeof( T ) > 1 ) {
						union _ {
							_( T value ) : value( value ) {}
							T		value;
							uint8_t bytes[sizeof( T )];
						} dec( value );
						for ( int i = 0; i < sizeof( T ); ++i ) cmd_caller.ptr[cmd_length++] = dec.bytes[i];
					} else
						cmd_caller.ptr[cmd_length++] = (uint8_t)value;
				}

				template<typename T, typename... Ts> void pusha( T value, Ts... values ) {
					push( value );
					if constexpr ( sizeof...( Ts ) > 1 )
						pusha( values... );
					else
						push( values... );
				}
			};

			template<class C> static Helper<C> *make_helper( C *obj, void ( C::*func )( const char * ) ) {
				return new Helper<C>( obj, func );
			}

			Command()				   = default;
			Command( const Command & ) = delete;
			Command( class Input *input );
			Command( const std::string &name, class Input *input = nullptr );
			Command( const std::string &name, const std::function<void( std::string_view )> &cb,
					 class Input *input = nullptr );
			virtual ~Command();

			virtual bool install();
			virtual bool install( const std::string &name );
			virtual bool install( const std::string &name, const std::function<void( std::string_view )> &cb );
			virtual bool install( const std::function<void( std::string_view )> &cb );

			virtual bool remove();

			virtual bool isInstalled() const;

			virtual bool rename( const std::string &name );
			virtual std::function<void( std::string_view )>
				redirect( const std::function<void( std::string_view )> &cb );

		protected:
			class Input *							_input = nullptr;
			std::string								_name;
			bool									_installed = false;
			Helper<Command> *						_helper	   = nullptr;
			std::function<void( std::string_view )> _callback  = []( std::string_view ) {};

			void command( const char *params );
		};

		enum class CmdFail { NOARGS, MANYARGS, FEWARGS, INVALIDARGS };
		template<typename... Args> class CommandEx : public Command {
		public:
			CommandEx()
				: Command(
					  "", [this]( std::string_view s ) { command_ex( s ); }, nullptr ) {}
			CommandEx( const Command & ) = delete;
			CommandEx( class Input *input )
				: Command(
					  "", [this]( std::string_view s ) { command_ex( s ); }, input ) {}
			CommandEx( const std::string &name, class Input *input = nullptr )
				: Command(
					  name, [this]( std::string_view s ) { command_ex( s ); }, input ) {}
			CommandEx( const std::string &name, const std::function<void( Args... )> &cb, class Input *input = nullptr )
				: Command(
					  name, [this]( std::string_view s ) { command_ex( s ); }, input ),
				  _callbackEx( cb ) {}

			virtual bool install() override { return Command::install(); }
			virtual bool install( const std::string &name ) override { return Command::install( name ); };
			virtual bool install( const std::string &name, const std::function<void( Args... )> &cb ) {
				_callbackEx = cb;
				return Command::install( name );
			}
			virtual bool install( const std::function<void( Args... )> &cb ) {
				_callbackEx = cb;
				return Command::install();
			}
			template<class C> bool install( C *obj, void ( C::*cb )( Args... ) ) {
				_callbackEx = [=]( Args... args ) { ( obj->*cb )( args... ); };
				return Command::install();
			}

			size_t countArgs() const { return sizeof...( Args ); }

			virtual std::function<void( Args... )> redirect( const std::function<void( Args... )> &cb ) {
				auto prevCb = _callbackEx;
				_callbackEx = cb;
				return prevCb;
			}
			template<class C> std::function<void( Args... )> redirect( C *obj, void ( C::*cb )( Args... ) ) {
				auto prevCb = _callbackEx;
				_callbackEx = [=]( Args... args ) { ( obj->*cb )( args... ); };
				return prevCb;
			}
			virtual std::function<void( CmdFail, size_t )>
				redirectFail( const std::function<void( CmdFail, size_t )> &cb ) {
				auto prevCb	  = _callbackFail;
				_callbackFail = cb;
				return prevCb;
			}
			template<class C>
			std::function<void( CmdFail, size_t )> redirectFail( C *obj, void ( C::*cb )( CmdFail, size_t ) ) {
				auto prevCb	  = _callbackFail;
				_callbackFail = [=]( CmdFail f, size_t a ) { ( obj->*cb )( f, a ); };
				return prevCb;
			}

		protected:
			enum class Type { BOOLEAN, INTEGRAL, FLOATING, STRING, INVALID };
			class item_exception : public std::runtime_error {
			public:
				explicit item_exception( const std::string &__arg, Type t ) noexcept
					: std::runtime_error( __arg ), type( t ) {}
				explicit item_exception( const char *__arg, Type t ) noexcept
					: std::runtime_error( __arg ), type( t ) {}
				item_exception( item_exception &&e ) noexcept : std::runtime_error( e ) { type = e.type; };
				item_exception( const item_exception &e ) noexcept : std::runtime_error( e ) { type = e.type; };

				Type type;
			};
			class exception : public item_exception {
			public:
				explicit exception( const std::string &__arg, Type t, size_t argId ) noexcept
					: item_exception( __arg, t ), argId( argId ) {}
				explicit exception( const char *__arg, Type t, size_t argId ) noexcept
					: item_exception( __arg, t ), argId( argId ) {}
				exception( exception &&e ) noexcept : item_exception( e ) { argId = e.argId; };
				exception( const exception &e ) noexcept : item_exception( e ) { argId = e.argId; };

				size_t argId;
			};

			std::function<void( Args... )>		   _callbackEx	 = []( Args... ) {};
			std::function<void( CmdFail, size_t )> _callbackFail = []( CmdFail, size_t ) {};

			void command_ex( std::string_view params ) {
				if constexpr ( sizeof...( Args ) ) {
					if ( params.empty() ) {
						_callbackFail( CmdFail::NOARGS, 0 );
						return;
					}
					std::vector<std::string_view> str;
					size_t						  pos = 0;
					while ( true ) {
						auto new_pos = params.find( ' ', pos );
						if ( pos != new_pos )
							str.push_back( params.substr( pos, new_pos ) );
						else if ( params.length() > pos )
							++pos;
						else
							break;
						if ( new_pos == std::string::npos ) break;
						pos = new_pos + 1;
					}
					if ( str.size() > sizeof...( Args ) ) {
						_callbackFail( CmdFail::MANYARGS, str.size() );
					} else if ( str.size() < sizeof...( Args ) ) {
						_callbackFail( CmdFail::FEWARGS, str.size() );
					} else {
						try {
							std::tuple<Args...> items;
							get_tuple( str, items );
							std::apply( _callbackEx, items );
						} catch ( exception &e ) {
							_callbackFail( CmdFail::INVALIDARGS, e.argId );
						}
					}
				} else
					_callbackEx();
			}

			template<typename T> void get_item( std::string_view str, T &arg ) {
				if constexpr ( std::is_same<T, bool>::value ) {
					try {
						if ( !stricmp( str.data(), "true" ) )
							arg = true;
						else if ( std::stoi( str.data() ) )
							arg = true;
						else
							arg = false;
					} catch ( std::exception &e ) {
						throw item_exception( e.what(), Type::BOOLEAN );
					}
				} else if constexpr ( std::is_integral<T>::value ) {
					try {
						if constexpr ( std::is_signed<T>::value ) {
							if constexpr ( std::is_same<T, long long>::value ) {
								arg = std::stoll( str.data() );
							} else {
								arg = std::stoi( str.data() );
							}
						} else {
							if constexpr ( std::is_same<T, unsigned long long>::value ) {
								arg = std::stoull( str.data() );
							} else {
								arg = std::stoul( str.data() );
							}
						}
					} catch ( std::exception &e ) {
						throw item_exception( e.what(), Type::INTEGRAL );
					}
				} else if constexpr ( std::is_floating_point<T>::value ) {
					try {
						if constexpr ( std::is_same<T, long double>::value ) {
							arg = std::stold( str.data() );
						} else if constexpr ( std::is_same<T, double>::value ) {
							arg = std::stod( str.data() );
						} else {
							arg = std::stof( str.data() );
						}
					} catch ( std::exception &e ) {
						throw item_exception( e.what(), Type::FLOATING );
					}
				} else if constexpr ( std::is_same<T, std::string_view>::value ||
									  std::is_same<T, std::string>::value ) {
					try {
						arg = str;
					} catch ( std::exception &e ) {
						throw item_exception( e.what(), Type::STRING );
					}
				} else
					throw item_exception( "Can't deserialize type", Type::INVALID );
			}
			template<size_t N = 0> void get_tuple( std::vector<std::string_view> &str, std::tuple<Args...> &t ) {
				if constexpr ( N < sizeof...( Args ) ) {
					try {
						get_item( str[N], std::get<N>( t ) );
					} catch ( item_exception &e ) {
						throw exception( e.what(), e.type, N );
					}
					get_tuple<N + 1>( str, t );
				}
			}
		};
		template<typename... Args>
		static CommandEx<Args...> *make_command( std::string name, void ( *cb )( Args... ) ) {
			return new CommandEx<Args...>( name, cb );
		}
		template<typename... Args>
		static CommandEx<Args...> make_command_s( std::string name, void ( *cb )( Args... ) ) {
			return CommandEx<Args...>( name, cb );
		}
		template<class C, typename... Args>
		static CommandEx<Args...> *make_command( std::string name, C *obj, void ( C::*cb )( Args... ) ) {
			return new CommandEx<Args...>( name, [=]( Args... args ) { ( obj->*cb )( args... ); } );
		}
		template<class C, typename... Args>
		static CommandEx<Args...> make_command_s( std::string name, C *obj, void ( C::*cb )( Args... ) ) {
			return CommandEx<Args...>( name, [=]( Args... args ) { ( obj->*cb )( args... ); } );
		}
	};

} // namespace SAMP

#endif // INPUT_CHAT_H
