#ifndef SAMP_PLAYERPOOL_H
#define SAMP_PLAYERPOOL_H

#include <string>

namespace SAMP {
	class PlayerPool {
		void *	 pPlayerPool;
		unsigned pRemotePlayers;

		class RemotePlayer *_remotePlayers	 = nullptr;
		short *				_localPlayerId	 = nullptr;
		int					_localPlayerNick = 0;

	public:
		PlayerPool( void *pPlayerPool );
		~PlayerPool();

		int *pListed = nullptr;

		class RemotePlayer *RemotePlayer( short id );
		short				localPlayerId();
		std::string			localPlayerNick();
	};
} // namespace SAMP

#endif // SAMP_PLAYERPOOL_H
