#ifndef SAMPVEHICLEPOOLS_H
#define SAMPVEHICLEPOOLS_H

class CVehicle;
namespace SAMP {
	class VehiclePool {
		void *pVehiclePool;

	public:
		VehiclePool( void *pool );

		int *	   pListed = nullptr;
		CVehicle **pVehs   = nullptr;
	};
} // namespace SAMP

#endif // SAMPVEHICLEPOOLS_H
