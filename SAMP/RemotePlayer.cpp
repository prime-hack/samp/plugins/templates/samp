#include "RemotePlayer.h"
#include "Library.h"
#include <stdexcept>
#include <windows.h>

SAMP::RemotePlayer::RemotePlayer( unsigned pRemotePlayer ) : pRemotePlayer( pRemotePlayer ) {
	switch ( Version() ) {
		case eVerCode::R1:
			ofRemotePlayerData = 0;
			ofPlayerId = 171;
			ofVehicleId = 173;
			ofSampPed = 0;
			ofGtaPed = 676;
			maxPlayers = 1004;
			ofAnimId = 0x1CE;
			ofIsNPC = 4;
			ofScore = 24;
			ofPing = 28;
			ofNickStruct = 12;
			ofShowNameTag = 179;
			ofArmour = 0x1B8;
			ofHealth = 0x1BC;
			ofAfk = 0x1D1;
			ofTeam = 8;
			break;
		case eVerCode::R4:
			[[fallthrough]];
		case eVerCode::R3:
			ofRemotePlayerData = 0;
			ofPlayerId = 8;
			ofVehicleId = 10;
			ofSampPed = 0;
			ofGtaPed = 676;
			maxPlayers = 1004;
			ofAnimId = 0x1CD;
			ofIsNPC = 4;
			ofScore = 24;
			ofPing = 28;
			ofNickStruct = 12;
			ofShowNameTag = 16;
			ofArmour = 0x1AC;
			ofHealth = 0x1B0;
			ofAfk = 0x1D1;
			ofTeam = 0x109;
			break;
		default:
			throw std::runtime_error( "Unsupported SAMP version" );
			break;
	}
}

short SAMP::RemotePlayer::playerId() const {
	if ( id == -1 ) return -1;
	auto data = pRemotePlayerData();
	if ( data == nullptr ) return -1;
	return *(short *)( (size_t)data + ofPlayerId );
}

short SAMP::RemotePlayer::vehicleId() const {
	if ( id == -1 ) return -1;
	auto data = pRemotePlayerData();
	if ( data == nullptr ) return -1;
	return *(short *)( (size_t)data + ofVehicleId );
}

CPed *SAMP::RemotePlayer::gtaPed() const {
	if ( id == -1 ) return *reinterpret_cast<CPed **>( 0xB6F5F0 );
	auto sampPed = ped();
	if ( sampPed != nullptr ) return *(CPed **)( (size_t)sampPed + ofGtaPed );
	return nullptr;
}

void *SAMP::RemotePlayer::ped() const {
	if ( id == -1 ) return nullptr;
	auto data = pRemotePlayerData();
	if ( data != nullptr ) return *(void **)( (size_t)data + ofSampPed );
	return nullptr;
}

int *SAMP::RemotePlayer::animId() {
	if ( id == -1 ) return nullptr;
	auto data = pRemotePlayerData();
	if ( data != nullptr ) return (int *)( (size_t)data + ofAnimId );
	return nullptr;
}

int SAMP::RemotePlayer::isNPC() const {
	if ( id == -1 ) return -1;
	if ( id >= 0 && id <= maxPlayers ) {
		auto remotePlayer = *(size_t *)( pRemotePlayer + id * 4 );
		if ( remotePlayer ) return *(int *)( remotePlayer + ofIsNPC );
	}
	return -1;
}

int SAMP::RemotePlayer::isNickShowed() const {
	if ( id == -1 ) return -1;
	auto data = pRemotePlayerData();
	if ( data == nullptr ) return -1;
	return *(int *)( (size_t)data + ofShowNameTag );
}

float SAMP::RemotePlayer::armour() const {
	if ( id == -1 ) return -1;
	auto data = pRemotePlayerData();
	if ( data == nullptr ) return -1;
	return *(float *)( (size_t)data + ofArmour );
}

float SAMP::RemotePlayer::health() const {
	if ( id == -1 ) return -1;
	auto data = pRemotePlayerData();
	if ( data == nullptr ) return -1;
	return *(float *)( (size_t)data + ofHealth );
}

bool SAMP::RemotePlayer::isAfk() const {
	if ( id == -1 ) return -1;
	auto data = pRemotePlayerData();
	if ( data == nullptr ) return -1;
	return *(int *)( (size_t)data + ofAfk ) == 2;
}

void SAMP::RemotePlayer::setAfk( bool state ) {
	if ( id == -1 ) return;
	auto data = pRemotePlayerData();
	if ( data == nullptr ) return;
	*(int *)( (size_t)data + ofAfk ) = ( state ? 2 : 0 );
}

std::uint8_t SAMP::RemotePlayer::team() const {
	if ( id == -1 ) return 0xFF;
	auto data = pRemotePlayerData();
	if ( data == nullptr ) return 0xFF;
	return *(std::uint8_t *)( (size_t)data + ofTeam );
}

void SAMP::RemotePlayer::team( std::uint8_t team ) {
	if ( id == -1 ) return;
	auto data = pRemotePlayerData();
	if ( data == nullptr ) return;
	*(std::uint8_t *)( (size_t)data + ofTeam ) = team;
}

int SAMP::RemotePlayer::score() const {
	if ( id == -1 ) return -1;
	if ( id >= 0 && id <= maxPlayers ) {
		auto remotePlayer = *(size_t *)( pRemotePlayer + id * 4 );
		if ( remotePlayer ) return *(int *)( remotePlayer + ofScore );
	}
	return -1;
}

int SAMP::RemotePlayer::ping() const {
	if ( id == -1 ) return -1;
	if ( id >= 0 && id <= maxPlayers ) {
		auto remotePlayer = *(size_t *)( pRemotePlayer + id * 4 );
		if ( remotePlayer ) return *(int *)( remotePlayer + ofPing );
	}
	return -1;
}

std::string SAMP::RemotePlayer::nick() const {
	if ( id == -1 ) return std::string();
	struct msvcString {
		union {
			char szString[16];
			char *pszString;
		};
		int length;
		int allocated;
	};
	if ( id >= 0 && id <= maxPlayers ) {
		auto remotePlayer = *(size_t *)( pRemotePlayer + id * 4 );
		if ( remotePlayer ) {
			auto str = *(msvcString *)( remotePlayer + ofNickStruct );
			if ( str.allocated < 0x10 ) return std::string( str.szString, str.length );
			return std::string( str.pszString, str.length );
		}
	}
	return std::string();
}

void *SAMP::RemotePlayer::pRemotePlayerData() const {
	if ( id == -1 ) return nullptr;
	if ( id >= 0 && id <= maxPlayers ) {
		auto remotePlayer = *(size_t *)( pRemotePlayer + id * 4 );
		if ( remotePlayer ) return *(void **)( remotePlayer + ofRemotePlayerData );
	}
	return nullptr;
}
