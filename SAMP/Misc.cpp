#include "Misc.h"
#include "Library.h"
#include <stdexcept>

SAMP::Misc *SAMP::Misc::self = nullptr;

SAMP::Misc::Misc() {
	switch ( Version() ) {
		case eVerCode::notLoaded:
			throw std::runtime_error( "SAMP not loaded" );
			break;
		case eVerCode::R1:
			pMisc			= reinterpret_cast<void *>( Library() + 0x21A10C );
			ofFpsLimit		= 0x69;
			ofCursorMode	= 0x55;
			ofCursorCounter = 0x59;
			pSwitchCursor	= reinterpret_cast<void( __thiscall * )( void *, Cursor, int )>( Library() + 0x9BD30 );
			break;
		case eVerCode::R3:
			pMisc			= reinterpret_cast<void *>( Library() + 0x26E8F4 );
			ofFpsLimit		= 0x5D;
			ofCursorMode	= 0x61;
			ofCursorCounter = 0x65;
			pSwitchCursor	= reinterpret_cast<void( __thiscall * )( void *, Cursor, int )>( Library() + 0x9FFE0 );
			break;
		case eVerCode::R4:
			pMisc			= reinterpret_cast<void *>( Library() + 0x26EA24 );
			ofFpsLimit		= 0x5D;
			ofCursorMode	= 0x61;
			ofCursorCounter = 0x65;
			pSwitchCursor	= reinterpret_cast<void( __thiscall * )( void *, Cursor, int )>( Library() + 0xA0720 );
			break;
		default:
			throw std::runtime_error( "Unsupported SAMP version" );
			break;
	}

	++refCounter;
}

SAMP::Misc *SAMP::Misc::Instance( bool no_create ) {
	if ( !self && !no_create ) self = new Misc();
	return self;
}

SAMP::Misc *SAMP::Misc::InstanceRef() {
	auto instance = Instance();
	if ( instance != nullptr ) ++instance->refCounter;
	return instance;
}

void SAMP::Misc::DeleteInstance( bool force ) {
	if ( !self ) return;
	--self->refCounter;
	if ( !self->refCounter || force ) {
		delete self;
		self = nullptr;
	}
}

int SAMP::Misc::fpsLimit() const {
	if ( !pMisc ) return false;
	return *(int *)( *(size_t *)pMisc + ofFpsLimit );
}

SAMP::Cursor SAMP::Misc::cursorMode() const {
	if ( !pMisc ) return Cursor::hide;
	return *(Cursor *)( *(size_t *)pMisc + ofCursorMode );
}

int SAMP::Misc::cursorCounter() const {
	if ( !pMisc ) return false;
	return *(int *)( *(size_t *)pMisc + ofCursorCounter );
}

void SAMP::Misc::switchCursor( Cursor mode ) {
	if ( !pMisc ) return;
	pSwitchCursor( *(void **)pMisc, mode, mode == Cursor::hide );
}
