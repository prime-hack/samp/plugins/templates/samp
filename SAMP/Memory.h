#ifndef MEMORY_H
#define MEMORY_H

class Packet;

namespace SAMP {
	class Memory {
		static Memory *self;
		int refCounter = 0;

		void( __cdecl *pFree )( void * ) = nullptr;
		void *( __cdecl *pCalloc )( int, int ) = nullptr;
		void *( __cdecl *pMalloc )( int ) = nullptr;
		void *( __cdecl *pRealloc )( void *, int ) = nullptr;

		void( __cdecl *pDeallocPacket )( Packet * ) = nullptr;
		Packet *( __cdecl *pAllocPacket )( int ) = nullptr;

		void *( __cdecl *pNew )( int ) = nullptr;
		void( __cdecl *pDelete )( void * ) = nullptr;

		Memory();
		~Memory() {}

	public:
		/**
		 * @brief Возвращает инстанс Memory SA:MP
		 * @details Если инстанс не существует и параметр \b no_create равен false, то создает новый инстанс
		 * @param no_create Пропустить создание инстанса, если он не существует
		 * @return инстанс Memory SA:MP
		 */
		[[nodiscard]] static Memory *Instance( bool no_create = false );
		/**
		 * @brief Возвращает ссылку на инстанс Memory SA:MP
		 * @details Поведение подобно вызову метода Instance с аргументами по умолчанию, но дополнительно инкрементируется счетчик ссылок
		 * @return инстанс Memory SA:MP
		 */
		[[nodiscard]] static Memory *InstanceRef();
		/**
		 * @brief Удаляет инстанс Memory SA:MP
		 * @details Если счетчик ссылок не пустой, то вместо удаления будет декрементирован счетчик. Для принудительного удаления инстанса, используйте force = true
		 * @param force Принудительное удаление
		 */
		static void DeleteInstance( bool force = false );

		/**
		 * @brief Вызывает free из libc, с который слинкован SAMP
		 * @param lpMem Указатель на память, аллоцированую через SAMP
		 */
		void free( void *lpMem );
		/**
		 * @brief Вызывает calloc из libc, с который слинкован SAMP
		 * @param count количество объектов
		 * @param size размер одного объекта
		 * @return указатель на выделенную память
		 */
		[[nodiscard]] void *calloc( int count, int size );
		/**
		 * @brief Вызывает malloc из libc, с который слинкован SAMP
		 * @param size количество памяти
		 * @return указатель на выделенную память
		 */
		[[nodiscard]] void *malloc( int size );
		/**
		 * @brief Вызывает realloc из libc, с который слинкован SAMP
		 * @param lpMem указатель на старый участок памяти
		 * @param size новое количество памяти
		 * @return указатель на перемещенную память
		 */
		[[nodiscard]] void *realloc( void *lpMem, int size );

		/**
		 * @brief Удаление пакета RakNet
		 * @param packet указатель на пакет
		 */
		void DeallocPacket( Packet *packet );
		/**
		 * @brief выделяет память для пакетов
		 * @param count количество пакетов
		 * @return указатель на выделенную память
		 */
		[[nodiscard]] Packet *AllocPacket( int count );

		/**
		 * @brief Выделяет память через new
		 * @param size количество памяти в байтах
		 * @return указатель на начало памяти
		 */
		[[nodiscard]] void *NewChars( int size );

		/**
		 * @brief Удаляет память через delete
		 * @param lpMem указатель на память
		 */
		void DeleteChars( void *lpMem );
	};
} // namespace SAMP

#endif // MEMORY_H
