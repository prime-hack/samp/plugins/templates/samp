#include <stdexcept>
#include <windows.h>

#include "Library.h"
#include "ObjectPool.h"
#include "PlayerPool.h"
#include "Pools.h"
#include "VehiclePool.h"

SAMP::Pools::Pools( void *pools ) : pPools( pools ) {
	switch ( Version() ) {
		case eVerCode::notLoaded:
			throw std::runtime_error( "SAMP not loaded" );
			break;
		case eVerCode::R1:
			ppVehicle	= reinterpret_cast<void **>( (size_t)pPools + 28 );
			ppPlayer	= reinterpret_cast<void **>( (size_t)pPools + 24 );
			ppObject	= reinterpret_cast<void **>( (size_t)pPools + 4 );
			ppTextdraws = reinterpret_cast<void **>( (size_t)pPools + 16 );
			break;
		case eVerCode::R4:
			[[fallthrough]];
		case eVerCode::R3:
			ppVehicle	= reinterpret_cast<void **>( (size_t)pPools + 12 );
			ppPlayer	= reinterpret_cast<void **>( (size_t)pPools + 8 );
			ppObject	= reinterpret_cast<void **>( (size_t)pPools + 20 );
			ppTextdraws = reinterpret_cast<void **>( (size_t)pPools + 32 );
			break;
		default:
			throw std::runtime_error( "Unsupported SAMP version" );
			break;
	}
}

SAMP::Pools::~Pools() {
	if ( _vehs ) delete _vehs;
	if ( _players ) delete _players;
	if ( _objects ) delete _objects;
}

SAMP::VehiclePool *SAMP::Pools::VehiclePool() {
	if ( *ppVehicle != nullptr ) {
		if ( !_vehs ) _vehs = new SAMP::VehiclePool( *ppVehicle );
		return _vehs;
	}
	return nullptr;
}

SAMP::PlayerPool *SAMP::Pools::PlayerPool() {
	if ( *ppPlayer != nullptr ) {
		if ( !_players ) _players = new SAMP::PlayerPool( *ppPlayer );
		return _players;
	}
	return nullptr;
}

SAMP::ObjectPool *SAMP::Pools::ObjectPool() {
	if ( *ppObject != nullptr ) {
		if ( !_objects ) _objects = new SAMP::ObjectPool( *ppObject );
		return _objects;
	}
	return nullptr;
}

void *SAMP::Pools::pTextdrawPool() {
	return *ppTextdraws;
}
