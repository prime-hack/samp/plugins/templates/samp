#ifndef SAMP_DXUT_H
#define SAMP_DXUT_H

class CDXUTDialog;
class CDXUTStatic;
class CDXUTButton;
class CDXUTCheckBox;
class CDXUTRadioButton;
class CDXUTComboBox;
class CDXUTSlider;
class CDXUTEditBox;
class CDXUTControl;

namespace SAMP {
	class DXUT {
		static DXUT *self;
		int			 refCounter = 0;

		int( __thiscall *pAddStatic )( CDXUTDialog *dialog, int ID, const char *strText, int x, int y, int w, int h,
									   bool bIsDefault, CDXUTStatic **ppCreated )			   = nullptr;
		int( __thiscall *pAddButton )( CDXUTDialog *dialog, int ID, const char *strText, int x, int y, int w, int h,
									   int nHotKey, bool bIsDefault, CDXUTButton **ppCreated ) = nullptr;
		int( __thiscall *pAddCheckBox )( CDXUTDialog *dialog, int ID, const char *strText, int x, int y, int w, int h,
										 bool bChecked, int nHotKey, bool bIsDefault,
										 CDXUTCheckBox **ppCreated )						   = nullptr;
		int( __thiscall *pAddRadioButton )( CDXUTDialog *dialog, int ID, int nButtonGroup, const char *strText, int x,
											int y, int w, int h, bool bChecked, int nHotKey, bool bIsDefault,
											CDXUTRadioButton **ppCreated )					   = nullptr;
		int( __thiscall *pAddComboBox )( CDXUTDialog *dialog, int ID, int x, int y, int w, int h, int nHotKey,
										 bool bIsDefault, CDXUTComboBox **ppCreated )		   = nullptr;
		int( __thiscall *pAddSlider )( CDXUTDialog *dialog, int ID, int x, int y, int w, int h, int min, int max,
									   int value, bool bIsDefault, CDXUTSlider **ppCreated )   = nullptr;
		int( __thiscall *pAddEditBox )( CDXUTDialog *dialog, int ID, const char *strText, int x, int y, int w, int h,
										bool bIsDefault, CDXUTEditBox **ppCreated )			   = nullptr;
		void( __thiscall *pRequestFocus )( CDXUTDialog *dialog, CDXUTControl *pControl )	   = nullptr;
		void( __thiscall *pClearFocus )()													   = nullptr;
		void( __thiscall *pRemoveControl )( CDXUTDialog *dialog, int ID )					   = nullptr;
		CDXUTControl *( __thiscall *pGetControl )( CDXUTDialog *dialog, int ID, int type )	   = nullptr;

		CDXUTControl *s_pControlFocus = nullptr;

		DXUT();
		~DXUT() {}

	public:
		/**
		 * @brief Возвращает инстанс DXUT SA:MP
		 * @details Если инстанс не существует и параметр \b no_create равен false, то создает новый инстанс
		 * @param no_create Пропустить создание инстанса, если он не существует
		 * @return инстанс DXUT SA:MP
		 */
		[[nodiscard]] static DXUT *Instance( bool no_create = false );
		/**
		 * @brief Возвращает ссылку на инстанс DXUT SA:MP
		 * @details Поведение подобно вызову метода Instance с аргументами по умолчанию, но дополнительно инкрементируется счетчик ссылок
		 * @return инстанс DXUT SA:MP
		 */
		[[nodiscard]] static DXUT *InstanceRef();
		/**
		 * @brief Удаляет инстанс DXUT SA:MP
		 * @details Если счетчик ссылок не пустой, то вместо удаления будет декрементирован счетчик. Для принудительного удаления инстанса, используйте force = true
		 * @param force Принудительное удаление
		 */
		static void DeleteInstance( bool force = false );

		/**
		 * @brief Добавляет статичный текст в диалог
		 * @param dialog диалог
		 * @param ID номер элемента (-1 для авто-вычисления)
		 * @param strText текст
		 * @param x позиция
		 * @param y позиция
		 * @param w размер
		 * @param h размер
		 * @param bIsDefault виджет по умолчанию
		 * @param ppCreated указатель на созданный виджет
		 * @return HRESULT-код возврата
		 */
		int AddStatic( CDXUTDialog *dialog, int ID, const char *strText, int x, int y, int w, int h,
					   bool bIsDefault = false, CDXUTStatic **ppCreated = nullptr );

		/**
		 * @brief Добавляет кнопку в диалог
		 * @param dialog диалог
		 * @param ID номер элемента (-1 для авто-вычисления)
		 * @param strText текст
		 * @param x позиция
		 * @param y позиция
		 * @param w размер
		 * @param h размер
		 * @param nHotKey клавиша-триггер основного события виджета
		 * @param bIsDefault виджет по умолчанию
		 * @param ppCreated указатель на созданный виджет
		 * @return HRESULT-код возврата
		 */
		int AddButton( CDXUTDialog *dialog, int ID, const char *strText, int x, int y, int w, int h, int nHotKey = 0,
					   bool bIsDefault = false, CDXUTButton **ppCreated = nullptr );

		/**
		 * @brief Добавляет чекбокс в диалог
		 * @param dialog диалог
		 * @param ID номер элемента (-1 для авто-вычисления)
		 * @param strText текст
		 * @param x позиция
		 * @param y позиция
		 * @param w размер
		 * @param h размер
		 * @param bChecked Отмечен по умолчанию
		 * @param nHotKey клавиша-триггер основного события виджета
		 * @param bIsDefault виджет по умолчанию
		 * @param ppCreated указатель на созданный виджет
		 * @return HRESULT-код возврата
		 */
		int AddCheckBox( CDXUTDialog *dialog, int ID, const char *strText, int x, int y, int w, int h,
						 bool bChecked = false, int nHotKey = 0, bool bIsDefault = false,
						 CDXUTCheckBox **ppCreated = nullptr );

		/**
		 * @brief Добавляет радио-клавишу в диалог
		 * @param dialog диалог
		 * @param ID номер элемента (-1 для авто-вычисления)
		 * @param nButtonGroup Группа радио-клавиш
		 * @param strText текст
		 * @param x позиция
		 * @param y позиция
		 * @param w размер
		 * @param h размер
		 * @param bChecked Отмечен по умолчанию
		 * @param nHotKey клавиша-триггер основного события виджета
		 * @param bIsDefault виджет по умолчанию
		 * @param ppCreated указатель на созданный виджет
		 * @return HRESULT-код возврата
		 */
		int AddRadioButton( CDXUTDialog *dialog, int ID, int nButtonGroup, const char *strText, int x, int y, int w,
							int h, bool bChecked = false, int nHotKey = 0, bool bIsDefault = false,
							CDXUTRadioButton **ppCreated = nullptr );

		/**
		 * @brief Добавляет комбобокс в диалог
		 * @param dialog диалог
		 * @param ID номер элемента (-1 для авто-вычисления)
		 * @param x позиция
		 * @param y позиция
		 * @param w размер
		 * @param h размер
		 * @param nHotKey клавиша-триггер основного события виджета
		 * @param bIsDefault виджет по умолчанию
		 * @param ppCreated указатель на созданный виджет
		 * @return HRESULT-код возврата
		 */
		int AddComboBox( CDXUTDialog *dialog, int ID, int x, int y, int w, int h, int nHotKey = 0,
						 bool bIsDefault = false, CDXUTComboBox **ppCreated = nullptr );

		/**
		 * @brief Добавляет слайдер в диалог
		 * @param dialog диалог
		 * @param ID номер элемента (-1 для авто-вычисления)
		 * @param x позиция
		 * @param y позиция
		 * @param w размер
		 * @param h размер
		 * @param min минимальное значение слайдера
		 * @param max максимальное значение слайдера
		 * @param value начальное значение слайдера
		 * @param bIsDefault виджет по умолчанию
		 * @param ppCreated указатель на созданный виджет
		 * @return HRESULT-код возврата
		 */
		int AddSlider( CDXUTDialog *dialog, int ID, int x, int y, int w, int h, int min = 0, int max = 100,
					   int value = 50, bool bIsDefault = false, CDXUTSlider **ppCreated = nullptr );

		/**
		 * @brief Добавляет поле ввода в диалог
		 * @param dialog диалог
		 * @param ID номер элемента (-1 для авто-вычисления)
		 * @param strText текст
		 * @param x позиция
		 * @param y позиция
		 * @param w размер
		 * @param h размер
		 * @param bIsDefault виджет по умолчанию
		 * @param ppCreated указатель на созданный виджет
		 * @return HRESULT-код возврата
		 */
		int AddEditBox( CDXUTDialog *dialog, int ID, const char *strText, int x, int y, int w, int h,
						bool bIsDefault = false, CDXUTEditBox **ppCreated = nullptr );

		/**
		 * @brief Переключает фокус на указанный элемент
		 * @param pControl элемент
		 */
		void RequestFocus( CDXUTControl *pControl );

		/**
		 * @brief Очищает фокус DXUT
		 */
		void ClearFocus();

		/**
		 * @brief Удаляет элемент из диалога
		 * @param dialog диалог
		 * @param ID id элемента
		 */
		void RemoveControl( CDXUTDialog *dialog, int ID );

		/**
		 * @brief Возвращает элемент диалога
		 * @param dialog диалог
		 * @param ID id элемента
		 * @param type тип элемента
		 * @return элемент
		 */
		CDXUTControl *GetControl( CDXUTDialog *dialog, int ID, int type );

		/**
		 * @brief Проверяет находится ли элемент в фокусе
		 * @param control элемент
		 * @return фокус
		 */
		bool IsFocused( CDXUTControl *control );
	};

} // namespace SAMP

#endif // SAMP_DXUT_H
