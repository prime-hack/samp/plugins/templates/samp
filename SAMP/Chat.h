#ifndef SAMP_CHAT_H
#define SAMP_CHAT_H

#include <string_view>

class CDXUTDialog;
class CDXUTScrollBar;
#ifdef _MSVC_VER
struct IDirect3DDevice9;
struct IDirect3DTexture9;
struct IDirect3DSurface9;
#else
class IDirect3DDevice9;
class IDirect3DTexture9;
class IDirect3DSurface9;
#endif

namespace SAMP {
	class Chat {
		static Chat *self;
		int refCounter = 0;

		void *pChat = nullptr;
		void *pAddMessage = nullptr;
		void *pAddInfoMessage = nullptr;
		void *pAddDebugMessage = nullptr;
		int ofDialog = 0;
		int ofScroll = 0;
		int ofUpdate = 0;
		int ofChatMode = 0;
		int ofChatBottom = 0;
		int ofPagesize = 0;
		int ofTimestamp = 0;
		int ofFonts = 0;
		int ofTexture = 0;
		int ofSurface = 0;
		int ofTimestampWidth = 0;
		int ofEntries = 0;
		int cEntryCount = 0;
		int cEntrySize = 0;
		int ofStringHeight = 0;

		Chat();
		~Chat() {}

	public:
		/**
		 * @brief Возвращает инстанс чата SA:MP
		 * @details Если инстанс не существует и параметр \b no_create равен false, то создает новый инстанс
		 * @param no_create Пропустить создание инстанса, если он не существует
		 * @return инстанс чата SA:MP
		 */
		[[nodiscard]] static Chat *Instance( bool no_create = false );
		/**
		 * @brief Возвращает ссылку на инстанс чата SA:MP
		 * @details Поведение подобно вызову метода Instance с аргументами по умолчанию, но дополнительно инкрементируется счетчик ссылок
		 * @return инстанс чата SA:MP
		 */
		[[nodiscard]] static Chat *InstanceRef();
		/**
		 * @brief Удаляет инстанс чата SA:MP
		 * @details Если счетчик ссылок не пустой, то вместо удаления будет декрементирован счетчик. Для принудительного удаления инстанса, используйте force = true
		 * @param force Принудительное удаление
		 */
		static void DeleteInstance( bool force = false );

		/**
		 * @brief Вывод информационного сообщения в чат
		 * @details Информационное сообщение имеет тип 4, а так же, по умолчанию, выводится зеленым цветом.
		 * @param msg сообщение
		 */
		void addMsgInfo( std::string_view msg ) const;
		/**
		 * @brief Вывод отладочного сообщения в чат
		 * @details Отладочное сообщение имеет тип 8, а так же, по умолчанию, выводится серым цветом.
		 * @param msg сообщение
		 */
		void addMsgDbg( std::string_view msg ) const;

		void addMsg( int type, std::string_view msg, std::string_view prefix, int color, int prefixColor ) const;

		/// Возвращает диалог
		[[nodiscard]] CDXUTDialog *dialog() const;

		/// Возвращает скролл бар чата
		[[nodiscard]] CDXUTScrollBar *scroll() const;

		/// Перерисовывает чат
		void updateRender() const;

		/// Возвращает текущий WindowMode
		[[nodiscard]] int chatWinMode() const;

		/**
		 * @brief Устанавливает WindowMode
		 * @details Автоматически вызывает перерисовку чата
		 * @param mode - режим чата [0-2]
		 */
		void chatWinMode( int mode );

		/// Возвращает нижнюю границу чата
		[[nodiscard]] int chatWinBottom() const;

		/// Возвращает размер чата
		[[nodiscard]] int pagesize() const;

		/**
		 * @brief Устанавливает размер чата
		 * @param size количество строк
		 */
		void pagesize( int size );

		/// Проверяет, включен ли таймстамп
		[[nodiscard]] bool isTimestampEnabled() const;

		/**
		 * @brief Переключает таймстамп
		 * @param state состояние
		 */
		void toggleTimestamp( bool state );

		/// Возвращает указатель на набор шрифтов
		[[nodiscard]] void *fonts() const;

		/// Возвращает указатель на текстуру чата
		[[nodiscard]] IDirect3DTexture9 *texture() const;

		/// Возвращает указатель на поверхность чата
		[[nodiscard]] IDirect3DSurface9 *surface() const;

		/// Возвращает ширину таймстампа
		[[nodiscard]] int timestampWidth() const;

		/**
		 * @brief Устанавливает ширину таймстампа
		 * @param size ширина в пикселях
		 */
		void timestampWidth( int size );

		/**
		 * @brief Возвращает указатель на строку чата
		 * @param id Номер строки
		 * @return Указатель на структуру строки
		 */
		[[nodiscard]] void *entry( int id ) const;

		/// Возвращает количество строк в чате
		[[nodiscard]] int entryCount() const;

		/// Возвращает высоту одной строки
		[[nodiscard]] int stringHeight() const;
	};

} // namespace SAMP

#endif // SAMP_CHAT_H
